<?php


namespace TPay\Client\API\API;


use Exception;
use TPay\Client\API\Urls\Urls;

class AppToMerchant
{
    /**
     * Process all app to merchant
     * account transfers here
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    public static function appToMerchant(array $options)
    {
        try {
            $response = json_decode((new TPayGateWay(Urls::$tpayBaseUrl))->processRequest(Urls::$app_to_merchant_url, (new TPayGateWay(Urls::$tpayBaseUrl))->setRequestOptions($options)));

            return $response;

        } catch (Exception $exception) {
            throw  new Exception($exception->getMessage());
        }
    }
}
