<?php


namespace TPay\Client\API\API;


use Exception;
use TPay\Client\API\Urls\Urls;

class SendMoney
{
    /**
     * Send money to accounts in
     * tpay here
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    public static function sendMoney(array $options)
    {
        try {
            $response = json_decode((new TPayGateWay(Urls::$tpayBaseUrl))->processRequest(Urls::$send_money_url, (new TPayGateWay(Urls::$tpayBaseUrl))->setRequestOptions($options)));

            return $response;

        } catch (Exception $exception) {
            throw  new Exception($exception->getMessage());
        }
    }
}
