<?php


namespace TPay\Client\API\API;


use Exception;
use TPay\Client\API\Urls\Urls;

class AppAccountTransfers
{
    /**
     * Here perform transfers
     * between the app accounts that
     * is balance/b2d/c2b of the app
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    public static function appAccountTransfers(array $options)
    {
        try {
            $response = json_decode((new TPayGateWay(Urls::$tpayBaseUrl))->processRequest(Urls::$app_transfers_url, (new TPayGateWay(Urls::$tpayBaseUrl))->setRequestOptions($options)));

            return $response;

        } catch (Exception $exception) {
            throw  new Exception($exception->getMessage());
        }
    }
}
