<?php


namespace TPay\Client\API\API;


use Exception;
use TPay\Client\API\Urls\Urls;

class MMFTransfers
{
    /**
     * Perform all transfers that have been requested
     * By the app b2c transfers here
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    public static function mmfTransfers(array $options)
    {
        try {
            $response = json_decode((new TPayGateWay(Urls::$tpayBaseUrl))->processRequest(Urls::$mmf_url, (new TPayGateWay(Urls::$tpayBaseUrl))->setRequestOptions($options)));

            return $response;

        } catch (Exception $exception) {
            throw  new Exception($exception->getMessage());
        }
    }
}
