<?php


namespace TPay\Client\API\API;


use Exception;
use TPay\Client\API\Urls\Urls;

class AccountTransfers
{
    /**
     * -----------------------------------
     * Process all account transfers here
     * -----------------------------------
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    public static function accountTransfers(array $options)
    {
        try {
            $response = json_decode((new TPayGateWay(Urls::$tpayBaseUrl))->processRequest(Urls::$account_transfers_url, (new TPayGateWay(Urls::$tpayBaseUrl))->setRequestOptions($options)));

            return $response;

        } catch (Exception $exception) {
            throw  new Exception($exception->getMessage());
        }
    }
}
