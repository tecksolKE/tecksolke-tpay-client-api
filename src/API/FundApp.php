<?php


namespace TPay\Client\API\API;


use Exception;
use TPay\Client\API\Urls\Urls;

class FundApp
{
    /**
     * Fund app here for the client
     * Only done by merchant account
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    public static function fundApp(array $options)
    {
        try {
            $response = json_decode((new TPayGateWay(Urls::$tpayBaseUrl))->processRequest(Urls::$fund_app_url, (new TPayGateWay(Urls::$tpayBaseUrl))->setRequestOptions($options)));

            return $response;

        } catch (Exception $exception) {
            throw  new Exception($exception->getMessage());
        }
    }
}
