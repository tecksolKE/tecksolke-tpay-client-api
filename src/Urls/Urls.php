<?php


namespace TPay\Client\API\Urls;


class Urls
{
    /**
     * Define the base url here
     * that is for both to mpesa
     * and only tpay
     */
    public static $mpesaBaseUrl = '/api/t-pay/v1/oauth2/';
    public static $tpayBaseUrl = '/api/t-pay/v1/oauth2/CbAKeK0dqjoAmcfwr0RvRpDJH6G6uqwXKmVuVOeAxKnoNhVflQ/';

    /**
     * -------------------------------------------------
     * Define all urls here for the t-pay m-pesa endpoints
     * ------------------------------------------------
     */
    public static $app_access_token_url = 'access-token';//This is Get Requests TODO This is shared in both tpay and to mpesa routes
    public static $app_balances_url = 'app-balances';//Get Request
    public static $app_c2b_stk_url = 'app-stk-deposit';//Post Request
    public static $app_c2b_url = '';//Post Request
    public static $app_b2c_url = 'app-b2c-withdraw';//Post Request


    /**
     * -------------------------------------------------
     * Define all urls here for the t-pay endpoints
     * ------------------------------------------------
     */
    public static $account_transfers_url = 'account-transfers';//This is Post Requests
    public static $app_to_merchant_url = 'app-to-merchant';//This is Post Requests
    public static $send_money_url = 'send-money';//This is Post Requests
    public static $fund_app_url = 'fund-app';//This is Post Requests
    public static $fund_client_url = 'fund-client';//This is Post Requests
    public static $app_transfers_url = 'app-transfers';//This is Post Requests
    public static $mmf_url = 'mmf';//This is Post Requests
    public static $express_payment_url = 'express-payment';//POST Request
}
