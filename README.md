# <p align="center"><a href="https://tpay.co.ke/" target="_blank"><img src="https://tpay.co.ke/img/logo-black.png"></a></p>

<p align="center">
  <b>Make It Happen</b><br>
  <img src="http://s.4cdn.org/image/title/105.gif">
</p>

#### This Package is Only For The T-Pay Application

T-Pay API is REST-API that makes it easy for one to make request to the T-Pay Payment Gateway for his/her App.

## Help and docs

- [API Documentation](https://dev.tpay.co.ke/)


## Installing

The recommended way to install tpay-client-api is through
[Composer](http://getcomposer.org).

```bash
# Install package via composer
composer require tecksolke-tpay/client-api
```

Next, run the Composer command to install the latest stable version of *tpay/client-api*:

```bash
# Update package via composer
 composer update tecksolke-tpay/client-api --lock
```

After installing, the package will be auto discovered, But if need you may run:

```php
# run for auto discovery <-- If the package is not detected automatically -->
composer dump-autoload
```

Then run this, to get the *config/tpay.php* for api configurations:

```php
# run this to get the configuartion file at config/tpay.php <-- read through it -->
php artisan vendor:publish --provider="TPay\Client\API\TPayClientServiceProvider"
```

You will have to provide this in the *.env* for the api configurations:

```php
# https://sandbox.tpay.co.ke
T_PAY_END_POINT_URL=

# TP4*****82F <-- keep this key secret -->
T_PAY_APP_KEY=

# <-- keep this code secret -->
T_PAY_APP_SECRET_CODE=

# 60 <-- The access token session lifetime is in minutes i.e 60 minutes --> ->default(58 minutes);
T_PAY_TOKEN_SESSION=

# 120 <-- Response timeout 120 seconds -->
# This is not a must you may choose to use the dafault value defined in the config/tpay.php;
T_PAY_RESPONSE_TIMEOUT=

# 60 <-- Connection timeout 60 seconds -->
# This is not a must you may choose to use the dafault value defined in the config/tpay.php;
T_PAY_CONNECTION_TIMEOUT=
```

## Usage
Follow the steps below on how to use the api:

#### How to use the API
This how the api will be accessed via this package...

```php

    /**
     * --------------------------
     * This Api are for all apps
     * --------------------------
     */

    /**
     * ---------------------------------
     *  Test for requesting app balance
     * ---------------------------------
     * @throws Exception
     */
    public function appBalance() {
        try {
            //Set request options as shown here
            $options = [
                'secretCode' => '',//This has to be your app T_PAY_APP_SECRET_CODE
            ];

            //make request here
            $response = AppBalances::appBalances($options);

            //continue with what you what to do with the $response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }

    /**
     * ------------------------------------
     * Making app stk push request for c2b
     * ------------------------------------
     */
    public function appC2BSTKPush() {
        try {
            //Set request options as shown here
            $options = [
                'secretCode' => '',//This has to be your app T_PAY_APP_SECRET_CODE
                'phoneNumber' => '',//The phone number has to be 2547xxxxxxx
                'referenceCode' => '',//The secret code should be unique in every request you send and must start with TPXXXX
                'amount' => 1,//Amount has to be an integer and less than or equal to KES 70000
                'resultURL' => '',//This has to be your callback i.e https://mydomain/callback or http://mydomain/callback
            ];

            //make the c2b stk push here
            $response = AppC2BSTKPush::appC2BSTKPush($options);

            //continue with what you what to do with the $response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }

    /**
     * ------------------------------------
     * Making app withdraw request for b2c
     * ------------------------------------
     */
    public function appB2C() {
        try {
            //Set request options as shown here
            $options = [
                'secretCode' => '',//This has to be your app T_PAY_APP_SECRET_CODE
                'phoneNumber' => '',//The phone number has to be 2547xxxxxxx
                'referenceCode' => '',//The secret code should be unique in every request you send and must start with TPXXXX
                'amount' => 1,//Amount has to be an integer and has to be greater than KES 10
                'resultURL' => '',//This has to be your callback i.e https://mydomain/callback or http://mydomain/callback
            ];

            //make the b2c withdraw here
            $response = AppB2C::appB2C($options);

            //continue with what you what to do with the $response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }

    /**
     * --------------------------
     * This Api is for only t-pay app
     * --------------------------
     */

    /**
     * -------------------
     * Account transfers
     * -------------------
     * This api is used for transferring funds from
     * one account to the other tha is
     * basic/single/merchant
     */
    public function accountTransfers() {
        try {
            $options = [
                'secretCode' => '',//The app secret code
                'accountNumberTo' => '',//This is the account to basic/single/merchant
                'accountNumberFrom' => '',//This is the account from basic/single/merchant
                'referenceCode' => '',//Unique referenceCode i.e TPXXXXX
                'amount' => '',//Amount i.e 10
            ];

            //end the request here
            $response = AppAccountTransfers::appAccountTransfers($options);

            //continue with what you what to do with the response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }

    /**
     * ------------------------
     * Send Money Here
     * -----------------------
     * Process send money too
     * different accounts
     */
    public function sendMoney() {
        try {
            $options = [
                'secretCode' => '',//The app secret code
                'accountNumberTo' => '',//This is the account to basic/single/merchant
                'accountNumberFrom' => '',//This is the account from basic/single/merchant
                'referenceCode' => '',//Unique referenceCode i.e TPXXXXX
                'amount' => '',//Amount i.e 10
            ];

            //send money here
            $response = SendMoney::sendMoney($options);

            //continue with what you what to do with the response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }

    /**
     * -------------------------
     * Fund Client Here
     * -------------------------
     * Process client funding to either
     * Basic/Single/Merchant Account
     */
    public function fundApp() {
        try {
            $options = [
                'secretCode' => '',//The app secret code
                'app_key' => '',//The app key for the app receiving the money
                'accountNumber' => '',//This is the account from basic/single/merchant
                'referenceCode' => '',//Unique referenceCode i.e TPXXXXX
                'amount' => '',//Amount i.e 10
            ];

            //send request here
            $response = FundApp::fundApp($options);

            //continue with what you what to do with the response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }

    /**
     * ------------------
     * Fund client Here
     * -----------------
     * Process client funding for both
     * Basic/Single/Merchant account
     */
    public function fundClient() {
        try {
            $options = [
                'secretCode' => '',//The app secret code
                'accountNumber' => '',//This is the account from basic/single/merchant
                'referenceCode' => '',//Unique referenceCode i.e TPXXXXX
                'amount' => '',//Amount i.e 10
            ];

            //process the client funding here
            $response = FundClient::fundClient($options);

            //continue with what you what to do with the response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }

    /**
     * --------------
     * App Transfers
     * --------------
     */
    public function appAccountTransfers() {
        try {
            $options = [
                'secretCode' => '',//The app secret code
                'app_key' => '',//App key for app performing transfers
                'accountNumber' => '',//Merchant account for the owner of the app
                'appAccountIdentifierFrom' => '',//Can be only from app balance or c2b_balance
                'appAccountIdentifierTo' => '',//Can only be b2c_balance of the app
                'referenceCode' => '',//Unique referenceCode i.e TPXXXXX
                'amount' => '',//Amount i.e 10
            ];

            //send request
            $response = AppAccountTransfers::appAccountTransfers($options);

            //continue with what you what to do with the response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }

    /**
     * -----------------
     * App to Merchant
     * -----------------
     * Process app to merchant here
     */
    public function appToMerchant() {
        try {
            $options = [
                'secretCode' => '',//The app secret code
                'app_key' => '',//App key for app performing transfers
                'accountNumber' => '',//The client account receiving the funds from the app
                'appAccountIdentifier' => '',//App balance or c2b_balance
                'referenceCode' => '',//Unique referenceCode i.e TPXXXXX
                'amount' => '',//Amount i.e 10
            ];

            //send request here
            $response = AppToMerchant::appToMerchant($options);

            //continue with what you what to do with the response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }

    /**
     * -----------------
     * MMF Transfers
     * -----------------
     * Move all request for b2c apps to
     * mmf account for m-pesa here
     */
    public function mmfTransfers() {
        try {
            $options = [
                'secretCode' => '',//The app secret code
                'MMF_ID' => '',//This MMF_ID has to be the one that has not been moved yet
            ];

            //process mmf
            $response = MMFTransfers::mmfTransfers($options);

            //continue with what you what to do with the response here
        } catch (Exception $exception) {
            //TODO If an exception occurs
        }
    }
   
        
         /**
             * ------------------
             * Express Payment
             * -----------------
             * This is used to directly get payment from
             * a client account to your application
             */
            public function expressPayment() {
                try {
                    $options = [
                        'secretCode' => '',//The app secret code
                        'accountNumber' => '',//The client account making the payment via express payment
                        'referenceCode' => '',//Unique referenceCode i.e TPXXXXX
                        'amount'=>//Should be at least 1 
                    ];
        
                    //make the request here
                    $response = ExpressPayment::expressPayment($options);
        
                    //proceed with the response
        
                } catch (Exception $exception) {
                    //TODO If an exception occurs
                }
            }
    
    
```

## Version Guidance

| Version | Status     | Packagist           | Namespace    | Repo                |
|---------|------------|---------------------|--------------|---------------------|
| 1.x     | Latest     | `tecksolke-tpay/client-api` | `TPay\Client\API` | [v1.7.7](https://gitlab.com/tecksolKE/tecksolke-tpay-client-api/-/tags/v1.7.7)|

[tpay-api-1-repo]: https://gitlab.com/tecksolKE/tecksolke-tpay-client-api

## Security Vulnerabilities
 For any security vulnerabilities, please email to [TecksolKE](mailto:client@tecksol.co.ke).
 
## License
 This package is open-source API licensed under the [MIT license](https://opensource.org/licenses/MIT).
